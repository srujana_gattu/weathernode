/**
 * A simple web server that always responds with a plain text greeting
 */
const http = require("http") 
const port = 8081

function requestListener(request, response) {
  response.write("Hello client!")  // write stuff to the response
  response.end()        // finish preparing the response
}

const server = http.createServer(requestListener)
server.listen(port)


// See all the availabe Node modules: https://nodejs.org/api/http.html
// Require the http module
// Set the port
// Define requestListener, a function automatically added to each request
// Create a server
// Start listening - pass in the port
// How do we call this? How do we make a request?
// How do we see our app?  What will appear? 
// Does this ever change?
